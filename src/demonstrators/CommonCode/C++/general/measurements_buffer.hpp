#ifndef MEASUREMENTS_BUFFER_HPP
#define MEASUREMENTS_BUFFER_HPP

#include <algorithm>
#include <numeric>
#include <vector>

/**
 * @brief MeasurementsBuffer is a buffer for measurements
 *
 * @tparam BufferType the type of values that are stored in the buffer
 */
template <typename BufferType>
class MeasurementsBuffer {
    static_assert(std::is_arithmetic<BufferType>::value, "Buffer type is not numeric!");

  public:
    MeasurementsBuffer() = default;
    ~MeasurementsBuffer() = default;

    void insert(const BufferType value);
    void clear();
    void size() { return _measurements.size(); }

    BufferType getAverage();
    BufferType getLowest();
    BufferType getHighest();

  private:
    std::vector<BufferType> _measurements;
};

/**
 * @brief inserts a value in the buffer
 */
template <typename BufferType>
void MeasurementsBuffer<BufferType>::insert(const BufferType value) {
    _measurements.push_back(value);
}

/**
 * @brief clears the buffer
 */
template <typename BufferType>
void MeasurementsBuffer<BufferType>::clear() {
    _measurements.clear();
}

/**
 * @brief returns the lowest value in the buffer
 */
template <typename BufferType>
BufferType MeasurementsBuffer<BufferType>::getLowest() {
    if (_measurements.size() == 0) {
        return {};
    }
    auto min = *std::min_element(_measurements.begin(), _measurements.end());
    return min;
}

/**
 * @brief returns the highest value in the buffer
 */
template <typename BufferType>
BufferType MeasurementsBuffer<BufferType>::getHighest() {
    if (_measurements.size() == 0) {
        return {};
    }

    auto max = *std::max_element(_measurements.begin(), _measurements.end());
    return max;
}

/**
 * @brief returns the average value in the buffer
 */
template <typename BufferType>
BufferType MeasurementsBuffer<BufferType>::getAverage() {
    auto size = _measurements.size();
    if (size == 0) {
        return {};
    }

    auto average = std::accumulate(_measurements.begin(), _measurements.end(), 0.0) / size;
    return average;
}

#endif