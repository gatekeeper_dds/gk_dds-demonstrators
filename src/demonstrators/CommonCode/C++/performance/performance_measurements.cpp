#include "performance_measurements.hpp"

/**
 * @brief start starts a time measurement
 *
 */
void PerformanceMeasurements::start() { _startingTime = std::chrono::high_resolution_clock::now(); }

/**
 * @brief stop stops a time measurement
 *
 */
void PerformanceMeasurements::stop() { _stoppedTime = std::chrono::high_resolution_clock::now(); }