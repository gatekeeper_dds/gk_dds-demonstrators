from sys import path
path.append("..")

from dds_library import topic, Writer, Domain, Key
import time
import logging
import ctypes

logging.getLogger().setLevel(logging.INFO)

# Creating the domain. Its ID must be the same for both the sender and
# receiver.
domain = Domain(1)

# Creating a class that will act as a topic. Use the @topic decorator
# with the specific domain.
@topic(domain)
class Data:
    def __init__(self, **kwargs):
        self.name   = str
        self.id     = Key(int)
        self.flag   = bool
    
    def __repr__(self):
        return f"| name: {self.name}, id: {self.id}, flag: {self.flag} |"

writer = Writer(Data)

c = 0
while True:
    # The topic can be instantiated with keyword arguments for the fields.
    # The fields have a default value, so it is not necessary to initilize
    # all of them. Here we don't initialize flag, so it will be set to False.
    sample = Data(name="msg", id=c)
    writer(sample)
    logging.info(f"{sample} sent.")
    time.sleep(2)
    c += 1
