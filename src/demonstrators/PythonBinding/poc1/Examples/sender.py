from sys import path
path.append("..")
from binding import Topic, Writer, Participant, DDSKeyValue
import time
import logging
import ctypes

logging.getLogger().setLevel(logging.INFO)

participant = Participant(27)
topic = Topic("testtopic", participant)
writer = Writer(participant, topic)


c = 0
while True:
    sample = DDSKeyValue(f"key: {c}".encode(), f"message {c}".encode())
    writer(sample)
    logging.info(f"{sample} sent.")
    time.sleep(2)
    c += 1