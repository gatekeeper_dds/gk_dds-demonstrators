#include <chrono>
#include <string>
#include <thread>

#include <iostream>

#include "flood.hpp"

/**
 * @brief Constructs a new flood object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the flood loop
 * @param floodMessages the number of messages that will be send / received
 * @param server_address the address of the MQTT server
 * @param qos the MQTT QoS level that should be used
 */
Flood::Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages, std::string server_address, const int qos)
    : _id{id}, _totalDevices{totalDevices}, _totalFloodMsg{floodMessages}, _running{false}, _message{false}, _floodSend{0},
    _floodReceived{0}, 
    _client{server_address, std::to_string(id)}, 
    _topicRead{_client, "flood" + std::to_string(id) , qos, false},
    _topicWrite{_client, (id == _totalDevices ? "flood1" : "flood" + std::to_string(id+1)) , qos, false}
    {
    initialise();
}

/**
 * @brief initialises the flood and the correct settings for DDS
 *
 */
void Flood::initialise() {
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(10);
    connOpts.set_mqtt_version(MQTTVERSION_5);

    auto tok = _client.connect(connOpts);
    tok->wait();
    _client.set_message_callback([this](mqtt::const_message_ptr msg) {
		_message = true;
	});

    _topicRead.subscribe();
}

/**
 * @brief runs the flood implementation
 *
 * @note this function could wait infinitely!
 */
void Flood::run() {
    _running = true;
    _timer.start();
    if (_id == 1) {
        runMaster();
    } else {
        runSlave();
    }
    _timer.stop();
}

/**
 * @brief runs the flood loop of the master
 *
 */
void Flood::runMaster() {
    // Initiate the flood
    while (_running) {
        if (_message) {
            if (_totalFloodMsg == ++_floodReceived) {
                _running = false;
            }
        }
        if (_totalFloodMsg != _floodSend) {
            _floodSend++;
            _topicWrite.publish(std::to_string(_floodSend));
        }
    }
}

/**
 * @brief runs the flood loop of the slave
 *
 */
void Flood::runSlave() {
    // Initiate the flood
    while (_running) {
        if (_message) {
            _floodReceived++;
            _topicWrite.publish(std::to_string(_floodSend));
            if (_totalFloodMsg == _floodReceived) {
                _running = false;
            }
        }
    }
}