#ifndef MATRIXBOARD_DISPLAY_H
#define MATRIXBOARD_DISPLAY_H

namespace matrixboard {

class MatrixBoardDisplay {
  public:
    MatrixBoardDisplay(const unsigned int maxMatrixboardValue = 999);
    ~MatrixBoardDisplay() = default;

    unsigned int getDisplayValue() const;
    void setDisplayValue(const unsigned int displayValue);

  private:
    unsigned int _displayValue;
    unsigned int _maxMatrixboardValue;
};

} // namespace matrixboard

#endif // MATRIXBOARD_DISPLAY_H