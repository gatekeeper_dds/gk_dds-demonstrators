#ifndef SENSORS_H
#define SENSORS_H

namespace matrixboard {

class CarSensor {

    public:
    /**
     * @brief Settings contains the settings for the CarSensor
     *
     * @param minimalCarAmount the minimum amount of cars that can be counted
     * @param maximumCarAmount the maximum amount of cars that can be counted
     */
    struct Settings {
        unsigned int minimalCarAmount = 0;
        unsigned int maximumCarAmount = 50;
    };
    CarSensor() = default;
    ~CarSensor() = default;

    unsigned int countedCars();
    void changeSettings(const Settings settings);
      
    private:
    Settings _settings;
};

} // namespace matrixboard

#endif