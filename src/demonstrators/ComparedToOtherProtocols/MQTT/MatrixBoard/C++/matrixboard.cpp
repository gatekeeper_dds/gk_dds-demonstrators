#include "matrixboard.hpp"

/**
 * @brief Construct a new Matrix Board:: Matrix Board object
 *
 * @param id the unique matrix board ID
 */
MatrixBoard::MatrixBoard(std::string id, std::string serverAddress)
    // standard port for mosquitto server
    : _client{_SERVER_ADDRESS, id}, _SERVER_ADDRESS{serverAddress}, _id{id}, _cars_self{0} {}

/**
 * @brief Destroy the Matrix Board:: Matrix Board object
 *
 */
MatrixBoard::~MatrixBoard() {}

/**
 * @brief Function to run the matrixboard logic
 *
 * Starts a matrixboard client
 * Sets last will message
 * Connects to server
 * Subscribes to matrixboardtopic
 * Sets message callback
 *
 * message callback
 *
 * Handles messages based on their content
 *
 */
void MatrixBoard::run() {
    mqtt::topic matrixboardtopic(_client, "matrixboards", 1, false);
    mqtt::topic selftopic(_client, _id, 1);
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(10);
    connOpts.set_mqtt_version(MQTTVERSION_3_1_1);

    auto lwt = mqtt::make_message("matrixboards", _id + "!", 2, false);
    connOpts.set_will_message(lwt);

    std::cout << "connecting..." << std::endl;
    auto tok = _client.connect(connOpts);
    tok->wait();
    std::cout << "connected" << std::endl;
    // Subscribe to the topic using "no local" so that
    // we don't get own messages sent back to us
    // sets no local to true
    auto subOpts = mqtt::subscribe_options(true);
    matrixboardtopic.subscribe(subOpts)->wait();
    matrixboardtopic.publish(_id + "?");
    _client.set_message_callback([this](mqtt::const_message_ptr msg) {
        PerformanceMeasurements rec_perf;
        rec_perf.start();
        std::cout << "message::" << msg->get_topic() << "::" << msg->get_payload_str() << std::endl;
        std::string topic = msg->get_topic();
        std::string payload = msg->get_payload();
        if (topic != "matrixboards") {
            MeasurementsFile performance_file("measurements.csv");
            std::size_t pos = payload.find("|");
            std::string id = payload.substr(0, pos);
            std::string value = payload.substr(pos+1, payload.length());
            _nextboards[std::stoi(topic)] = std::stoi(value);
            std::vector<std::string> measurements{id, topic, _id, topic, value, rec_perf.getStartMili()};
            performance_file.write(measurements);
        } else {
            if (payload.find("!") != std::string::npos) {
                _nextboards.erase(std::stoi(payload.substr(0, payload.size() - 1)));
                _erase = std::stoi(payload.substr(0, payload.size() - 1));
                if (std::stoi(payload.substr(0, payload.size() - 1)) < std::stoi(_id)) {
                    _notify_liveliness = true;
                }
            } else if (payload.find("?") != std::string::npos) {
                processNewBoard(payload.substr(0, payload.size() - 1));
            } else {
                if (payload != _id && std::stoi(payload) > std::stoi(_id)) {
                    processNewBoard(payload);
                }
            }
        }
    });
    MeasurementsFile performance_file("measurements.csv");
    PerformanceMeasurements send_perf;
    int message_id = 0;

    while (true) {
        _cars_self = _sensor.countedCars();
        send_perf.start();
        selftopic.publish(std::to_string(message_id)+ "|" +std::to_string(_cars_self));
        std::vector<std::string> measurements{std::to_string(message_id), _id, "-1", _id, std::to_string(_cars_self), send_perf.getStartMili()};
        performance_file.write(measurements);
        message_id++;
        if (_notify_liveliness) {
            matrixboardtopic.publish(_id);
            _notify_liveliness = false;
        }
        if (_erase != -1 && _nextboards.count(_erase) > 0) {
            removeBoardFromSubscriptions();
        }

        if (_change) {
            subscribeToBoards();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
    }
}

void MatrixBoard::removeBoardFromSubscriptions() {
    std::cout << _id << ":unsub:" << _erase << std::endl;
    _client.unsubscribe(std::to_string(_erase));
    _erase = -1;
}

void MatrixBoard::subscribeToBoards() {
    if (_nextboards.size() < 2) {
        std::cout << _id << ":sub:" << _nextboards.begin()->first << std::endl;
        _client.subscribe(std::to_string(_nextboards.begin()->first), 2);
        _change = false;
    } else {
        std::cout << _id << ":sub:" << _nextboards.begin()->first << ":and:" << (--_nextboards.end())->first
                  << std::endl;
        _client.subscribe(std::to_string(_nextboards.begin()->first), 2);
        _client.subscribe(std::to_string((--_nextboards.end())->first), 2);
        _change = false;
    }
}

/**
 * @brief Processes new board
 *
 * subscribes to the new board topic if this is needed.
 *
 * @param string new_board
 */
void MatrixBoard::processNewBoard(std::string new_board) {
    if (std::stoi(new_board) > std::stoi(_id)) {
        std::lock_guard<std::mutex> guard(_boards_mutex);
        int newone = std::stoi(new_board);
        std::cout << "nb:" << new_board << std::endl;
        if (_nextboards.size() < 2) {
            _nextboards[newone] = 0;
            _change = true;
        } else {
            int second = (--_nextboards.end())->first;
            std::cout << "sec: " << second << "new:" << newone << std::endl;
            if (newone < second) {
                _erase = second;
                _nextboards[newone] = 0;
                _nextboards.erase(second);
                std::cout << "erased" << std::endl;
                _change = true;
            }
        }
    } else {
        _notify_liveliness = true;
    }
}
