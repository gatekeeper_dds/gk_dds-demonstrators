#include "matrixboard.hpp"

std::string getMatrixBoardIdFromUser();

int main(int argc, const char **argv) {
    std::string id = getMatrixBoardIdFromUser();
    std::cout << "construct board: " << id << std::endl;
    MatrixBoard board(id, "tcp://localhost:1883");
    std::cout << "start run" << std::endl;
    try {
        board.run();
    } catch (const std::exception &e) {
        std::cerr << "Error caught in main: " << e.what();
        return -1;
    }

    return 0;
}

std::string getMatrixBoardIdFromUser() {
    std::string id;
    std::cout << "Type a number: ";                     // Type a number and press enter
    std::cin >> id;                                     // Get user input from the keyboard
    std::cout << "Your number is: " << id << std::endl; // Display the input value

    return id;
}