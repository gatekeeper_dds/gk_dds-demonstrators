#include <chrono>
#include <string>
#include <thread>

#include "roundtrip.hpp"

/**
 * @brief Construct a new Round Trip:: Round Trip object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the roundtrip
 * @param roundTrips the number of roundtrips that will be executed
 * @param pubAddress Address of socket to publish to
 * @param subAddress Address of socket to subscribe to
 */
RoundTrip::RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips, const std::string pubAddress, const std::string subAddress)
    : _id{id}, _totalDevices{totalDevices}, _totalRoundtrips{roundTrips}, _running{false}, _roundTrips{0}, _subAddress{subAddress}, _pubAddress{pubAddress},
    _writeTopic{_id == _totalDevices ? "roundtrip1" : "roundtrip" + std::to_string(_id + 1)}, _readTopic{"roundtrip" + std::to_string(_id)},
    _context{}, _subSocket{_context, ZMQ_SUB}, _pubSocket{_context, ZMQ_PUB} {
    initialise();
}

/**
 * @brief Destroy the Round Trip:: Round Trip object including allocated memory
 *
 */
RoundTrip::~RoundTrip() {
}

/**
 * @brief initialise initialises the round trip and the correct settings for DDS
 *
 */
void RoundTrip::initialise() {
    _subSocket.connect(_subAddress);
    _pubSocket.bind(_pubAddress);
    // Sleep for initialisation
    _subSocket.setsockopt(ZMQ_SUBSCRIBE, _readTopic.c_str(), 1);
    std::cout << "\n" << _id << " sub to:" << _subAddress << " pub to:" << _pubAddress << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));//sleep to settel connection
}

/**
 * @brief run runs the roundtrip
 *
 * @note this function could wait infinitely!
 */
void RoundTrip::run() {
    zmq::message_t msg, topic;
    zmq::recv_result_t c1, c2;
    _running = true;
    zmq::pollitem_t items[] = {{_subSocket, 0, ZMQ_POLLIN, 0}};
    std::cout << _id << " started" << std::endl;
    _timer.start();
    // Initiate the roundtrip
    if (_id == 1) {;
        _pubSocket.send(generateMessageFromString(_writeTopic), zmq::send_flags::sndmore);
        _pubSocket.send(generateMessageFromString(std::to_string(_id)), zmq::send_flags::none);
    }
    while (_running) {
        zmq::poll(&items[0], 1);  
        if(items[0].revents & ZMQ_POLLIN) {
            c1 = _subSocket.recv(topic, zmq::recv_flags::none);  
            c2 = _subSocket.recv(msg, zmq::recv_flags::none);
            _pubSocket.send(generateMessageFromString(_writeTopic), zmq::send_flags::sndmore);
            _pubSocket.send(generateMessageFromString(std::to_string(_id)), zmq::send_flags::none);
        }
        if (_totalRoundtrips == ++_roundTrips) {
            _running = false;
        }      
    }
    _timer.stop();
    std::cout << _id << " done" << std::endl;
}

/**
 * @brief Function to turn a string into a zmq message
 *
 * @param message string to be sent
 * @return zmq::message_t zmq message
 */
zmq::message_t RoundTrip::generateMessageFromString(std::string message) {
    zmq::message_t msg(message.size());
    memcpy(msg.data(), message.c_str(), message.size());
    return msg;
}