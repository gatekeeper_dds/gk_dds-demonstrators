#ifndef FLOOD_H
#define FLOOD_H

#include "performance/performance_measurements.hpp"
#include <zmq.hpp>

/**
 * @brief Flood contains the implementation of a flood loop with DDS
 *
 */
class Flood {
  public:
    Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages, const std::string pubAddress, const std::string subAddress);
    ~Flood();

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    zmq::message_t generateMessageFromString(std::string message);

    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalFloodMsg;

    bool _running;
    unsigned int _floodSend;
    unsigned int _floodReceived;

    const std::string _subAddress;
    const std::string _pubAddress;
    std::string _writeTopic;
    std::string _readTopic;

    zmq::context_t _context;
    zmq::socket_t _subSocket;
    zmq::socket_t _pubSocket;

    void *_messageMemory[1];

    PerformanceMeasurements _timer;

    void initialise();
    void runMaster();
    void runSlave();
};

#endif // FLOOD_H