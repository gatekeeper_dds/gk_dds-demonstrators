#include "matrixboard_display.hpp"

namespace matrixboard {

/**
 * @brief Construct a new Matrix Board Display:: Matrix Board Display object
 *
 * @param maxMatrixboardValue the maximum value the matrixboard can show
 */
MatrixBoardDisplay::MatrixBoardDisplay(const unsigned int maxMatrixboardValue)
    : _displayValue{120}, _maxMatrixboardValue{maxMatrixboardValue} {}

/**
 * @brief getDisplayValue returns the speed "shown" on the matrixboard
 *
 * @return unsigned int the speed on the matrixboard
 */
unsigned int MatrixBoardDisplay::getDisplayValue() const { return _displayValue; }

/**
 * @brief setDisplayValue sets the number that is "shown" on the matrixboard
 *
 * @param displayValue the value that is set on the matrixboard
 */
void MatrixBoardDisplay::setDisplayValue(const unsigned int displayValue) {
    // Maximum number that can be written on matrixboards
    if (_displayValue > _maxMatrixboardValue) {
        return;
    }
    _displayValue = displayValue;
}

} // namespace matrixboard