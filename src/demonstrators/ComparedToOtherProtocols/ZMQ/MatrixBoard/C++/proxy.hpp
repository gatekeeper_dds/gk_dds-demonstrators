#ifndef PROXY_H
#define PROXY_H

#include <iostream>
#include <string>
#include <zmq.hpp>
class Proxy {

  public:
    Proxy(std::string serverAddress, std::string backendPort, std::string frontendPort);
    ~Proxy() = default;

    void start();

  private:
    const std::string _SERVER_ADDRESS;
    const std::string _SUB_PORT;
    const std::string _PUB_PORT;
    zmq::context_t *_context;
    zmq::socket_t *_subscriber;
    zmq::socket_t *_publisher;
};

#endif