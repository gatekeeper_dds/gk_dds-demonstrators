#include "measurements_file.hpp"
#include <iostream>

inline bool exists (const std::string& name);

int main(int argc, char const *argv[]) {
    int removed_send, removed_incoming;
    removed_send = remove("measurements.csv");
    if(removed_send != 0){
        std::cout << "measurements file could not be deleted." << std::endl;
    }
    MeasurementsFile performanceFile("measurements.csv");
    std::vector<std::string> measurements{"message_id", "sent_by", "received_by", "topic", "Message", "milli"};
    performanceFile.createFile(measurements);

    return 0;
}
inline bool exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }   
}