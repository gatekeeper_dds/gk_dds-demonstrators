#!/bin/bash
#request reply with broker


HOST="192.168.0.202"

Pi0="192.168.0.200"
Pi1="192.168.0.201"
Pi2="192.168.0.201"
Pi3="192.168.0.203"


REPLY="tcp://192.168.0.201:5560"

RECBASE="tcp://*:55"
SENDBASE="tcp://localhost:55"

trap ctrl_c INT

ctrl_c() {
		echo "knock knock, cleaning lady"
       	sh cleanup.sh
}

# copy MQTT.py the raspberries 
scp broker.py 	pi@${Pi0}:~/broker.py
scp broker2.py 	pi@${Pi1}:~/broker2.py
scp	rep.py 		pi@${Pi3}:~/rep.py


python3 broker.py 	${RECBASE}59 	${SENDBASE}70 	& #Pi0 basicly the same as the standard broker from the zmq website except it sends request through to broker2
sleep 1
python3 broker2.py 	${RECBASE}70	${SENDBASE}60	& #Pi1
sleep 1
#ssh pi@${Pi3} python3 rep.py		${REPLY}  &	#Pi3
sleep 1
python3 req.py 		${SENDBASE}59	#Pi2


sh cleanup.sh