#
#   Request-reply service in Python
#   Connects REP socket to tcp://localhost:5560
#   Expects "Hello" from client, replies with "World"
#
import zmq
import sys
print("repping")
ReplyUrl=         sys.argv[1]         if len(sys.argv) > 1 else "tcp://localhost:5560"
print("Replying to %s" % ReplyUrl)


context = zmq.Context()
socket = context.socket(zmq.REP)
socket.connect(ReplyUrl)

while True:
    message = socket.recv()
    print("Received request: %s" % message)
    socket.send(message)