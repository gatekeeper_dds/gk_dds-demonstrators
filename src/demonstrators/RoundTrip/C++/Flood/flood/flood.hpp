#ifndef FLOOD_H
#define FLOOD_H

#include "performance/performance_measurements.hpp"

#include "FloodData.h"
#include "dds/dds.h"

/**
 * @brief Flood contains the implementation of a flood loop with DDS
 *
 */
class Flood {
  public:
    Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages);
    ~Flood();

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalFloodMsg;

    bool _running;
    unsigned int _floodSend;
    unsigned int _floodReceived;
    FloodData_Msg _msg;
    dds_entity_t _writer, _reader;
    dds_entity_t _participant;
    dds_entity_t _topicWrite, _topicRead;
    void *_messageMemory[1];

    PerformanceMeasurements _timer;

    void initialise();
    void runMaster();
    void runSlave();
};

#endif // FLOOD_H