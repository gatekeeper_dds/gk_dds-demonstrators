#include <chrono>
#include <string>
#include <thread>

#include "roundtrip.hpp"

/**
 * @brief Construct a new Round Trip:: Round Trip object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the roundtrip
 * @param roundTrips the number of roundtrips that will be executed
 */
RoundTrip::RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips)
    : _id{id}, _totalDevices{totalDevices}, _totalRoundtrips{roundTrips}, _running{false}, _roundTrips{0} {
    initialise();
}

/**
 * @brief Destroy the Round Trip:: Round Trip object including allocated memory
 *
 */
RoundTrip::~RoundTrip() {
    dds_delete(_participant);
    RoundTripData_Msg_free(_messageMemory[0], DDS_FREE_ALL);
}

/**
 * @brief initialise initialises the round trip and the correct settings for DDS
 *
 */
void RoundTrip::initialise() {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    std::string readTopic = "roundtrip" + std::to_string(_id);
    std::string writeTopic = "roundtrip" + std::to_string(_id + 1);
    if (_id == _totalDevices) {
        writeTopic = "roundtrip1";
    }

    dds_qos_t *qos = dds_create_qos();
    dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(5));
    _topicWrite = dds_create_topic(_participant, &RoundTripData_Msg_desc, writeTopic.c_str(), NULL, NULL);
    _topicRead = dds_create_topic(_participant, &RoundTripData_Msg_desc, readTopic.c_str(), NULL, NULL);
    _writer = dds_create_writer(_participant, _topicWrite, NULL, NULL);
    _reader = dds_create_reader(_participant, _topicRead, qos, NULL);

    dds_delete_qos(qos);
    _msg.key = static_cast<int32_t>(_id);

    _messageMemory[0] = RoundTripData_Msg__alloc();
    // Sleep for initialisation
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

/**
 * @brief run runs the roundtrip
 *
 * @note this function could wait infinitely!
 */
void RoundTrip::run() {
    dds_return_t readCheck;
    dds_sample_info_t infos[1];

    _running = true;
    _timer.start();
    // Initiate the roundtrip
    if (_id == 1) {
        dds_write(_writer, &_msg);
    }
    while (_running) {
        readCheck = dds_take(_reader, _messageMemory, infos, 1, 1);
        if ((readCheck > 0) && (infos[0].valid_data)) {
            dds_write(_writer, &_msg);
            if (_totalRoundtrips == ++_roundTrips) {
                _running = false;
            }
        }
    }
    _timer.stop();
    _running = false;
}