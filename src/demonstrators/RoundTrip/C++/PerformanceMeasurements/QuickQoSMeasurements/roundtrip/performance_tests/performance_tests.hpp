#ifndef PERFORMANCE_TESTS_H
#define PERFORMANCE_TESTS_H

#include "measurements_file/measurements_file.hpp"
#include "qos_manager/qos_manager.hpp"
#include "roundtrip/roundtrip.hpp"
#include <string>

/**
 * @brief Contains a lot of different DDS performance tests that can be executed on the round trip
 * It also stores the results in a file (each result will be appended to the file)
 *
 */
class PerformanceTests {
  public:
    PerformanceTests(const unsigned int id, const unsigned int totalDevices, const unsigned int roundtrips,
                     std::string filename = "measurements.csv");
    ~PerformanceTests() = default;

    void start();

  private:
    const unsigned int _id;
    const unsigned int _totalRoundtrips;
    const unsigned int _totalDevices;

    MeasurementsFile _measurementsFile;
    QoSManager _qosConfig;

    void globalTests();
    void qosPolicyTests();
    void executeTest(const std::string &testName, QoSManager *qosConfig);
};

#endif // PERFORMANCE_TESTS_H