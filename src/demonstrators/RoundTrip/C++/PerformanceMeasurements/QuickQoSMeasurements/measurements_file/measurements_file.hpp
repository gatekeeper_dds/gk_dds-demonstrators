#ifndef MEASUREMENTS_FILE_H
#define MEASUREMENTS_FILE_H

#include <fstream>
#include <sstream>
#include <string>

/**
 *  @brief MeasurementsFile writes new measurements into the measurements file
 *  The new measurements are just appended to the file.
 *  This could still be optimized to classify the measurements and place them together
 */
class MeasurementsFile {
  public:
    explicit MeasurementsFile(const std::string &fileName = "measurements.csv");
    ~MeasurementsFile() = default;

    template <typename InputType>
    void write(const std::string &measurementName, const InputType performance);

  private:
    const std::string _fileName;
};

/** @brief write appends a new measurement in the measurements file
 *
 * @param measurementName the name in front of the measurement result
 * @param performance the performance that is measured and will be stored in the measurements file
 */
template <typename InputType>
void MeasurementsFile::write(const std::string &measurementName, const InputType performance) {
    std::fstream measurementsFile(_fileName, std::fstream::in | std::fstream::out | std::fstream::app);

    measurementsFile << measurementName << "," << performance << "\n";

    measurementsFile.close();
}

#endif // MEASUREMENTS_FILE_H