#include "qos_manager.hpp"

/**
 * @brief Constructs a new QoSRoundtrip object and allocates the necessary memory
 *
 */
QoSManager::QoSManager() {
    allocateDefaultQoS();
    allocateCustomQoS();
}

/**
 * @brief Destroy the QoSRoundtrip object and deallocates the allocated memory
 *
 */
QoSManager::~QoSManager() {
    deallocateCustomQoS();
    deallocateDefaultQoS();
}

/**
 * @brief configures the QoS function for the right QoS objects / listener
 *
 * @param configurationFunction the function containing the wanted configuration, as well as containing for whom it is
 * meant
 */
void QoSManager::configureQoS(dds_qos_t *qosWriter, dds_qos_t *qosReader, dds_qos_t *qosWriteTopic,
                              dds_qos_t *qosReadTopic, dds_listener_t *listener, QoSFunction configurationFunction) {
    if (configurationFunction.writer) {
        configurationFunction.function(qosWriter, listener, configurationFunction.configuration);
    }
    if (configurationFunction.reader) {
        configurationFunction.function(qosReader, listener, configurationFunction.configuration);
    }
    if (configurationFunction.writeTopic) {
        configurationFunction.function(qosWriteTopic, listener, configurationFunction.configuration);
    }
    if (configurationFunction.readTopic) {
        configurationFunction.function(qosReadTopic, listener, configurationFunction.configuration);
    }
}

/**
 * @brief deallocates the default QoS objects and then allocates them again
 * This way, the default configuration is back to the default of Cyclone DDS
 */
void QoSManager::resetDefaultQoS() {
    deallocateDefaultQoS();
    allocateDefaultQoS();
}

/**
 * @brief allocates the default QoS objects + listener
 *
 */
void QoSManager::allocateDefaultQoS() {
    _defaultWriter = dds_create_qos();
    _defaultReader = dds_create_qos();
    _defaultWriteTopic = dds_create_qos();
    _defaultReadTopic = dds_create_qos();
    _defaultListener = dds_create_listener(NULL);
}

/**
 * @brief deallocates the default QoS objects + listener
 *
 */
void QoSManager::deallocateDefaultQoS() {
    dds_delete_qos(_defaultWriter);
    dds_delete_qos(_defaultReader);
    dds_delete_qos(_defaultWriteTopic);
    dds_delete_qos(_defaultReadTopic);
    dds_delete_listener(_defaultListener);
}

/**
 * @brief allocates the custom QoS objects + listener
 *
 */
void QoSManager::allocateCustomQoS() {
    _customWriter = dds_create_qos();
    _customReader = dds_create_qos();
    _customWriteTopic = dds_create_qos();
    _customReadTopic = dds_create_qos();
    _customListener = dds_create_listener(NULL);
}

/**
 * @brief deallocates the custom QoS objects
 *
 */
void QoSManager::deallocateCustomQoS() {
    dds_delete_qos(_customWriter);
    dds_delete_qos(_customReader);
    dds_delete_qos(_customWriteTopic);
    dds_delete_qos(_customReadTopic);
    dds_delete_listener(_customListener);
}

/**
 * @brief copies the default QoS objects and listener in the custom ones
 *
 */
void QoSManager::copyDefaultToCustom() {
    dds_copy_qos(_customWriter, _defaultWriter);
    dds_copy_qos(_customReader, _defaultReader);
    dds_copy_qos(_customWriteTopic, _defaultWriteTopic);
    dds_copy_qos(_customReadTopic, _defaultReadTopic);
    dds_copy_listener(_customListener, _defaultListener);
}