#ifndef __PING_PONG_HPP__
#define __PING_PONG_HPP__
#include "dds/dds.h"
#include "MSGData.h"
#include <iostream>
#include <string> 
#include <cstring> 
/**
 * @brief header file of PingPong class
 */
class PingPong{    
public:
    //constructors/destructors
    PingPong(int ID, int pingpongmodifier);
    virtual ~PingPong();
    //functions
    bool read();
    void write(std::string message);
    //getters
    dds_entity_t getWaitSet() const;
private:
    //variables
    int _id;
    dds_entity_t _dds_participant;
    dds_entity_t _waitSet;
    dds_entity_t _readtopic;
    dds_entity_t _writetopic;
    dds_entity_t _reader;
    dds_entity_t _writer; 
    dds_listener_t *_listener;
    //functions
    static void data_available(dds_entity_t reader, void *arg);
};
#endif