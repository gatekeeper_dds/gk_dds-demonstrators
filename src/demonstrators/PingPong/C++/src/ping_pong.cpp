 #include "ping_pong.hpp"
/**
 * @brief Construct a new PingPong:: PingPong object
 * 
 * creates a participant for the class
 * creates read and write topics based on the id op the pong object
 * creates a reader and a writer for the topic
 * creates a waitset to use to wait for a message (only for pong)
 * creates a listener to listen to the data_available event (only for ping)
 * 
 * @param id The id of the object
 * @param pingpongmodifier should be 1 or -1 specifies if the object readtopic should be 1 higher or lower than its own id (ping 1 higher, pong 1 lower)
 */
PingPong::PingPong(int id, int pingpongmodifier):
   _dds_participant{dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL)},
   _writetopic{dds_create_topic(_dds_participant, &MSGData_Msg_desc, ("pingpong"+std::to_string(id+pingpongmodifier)).c_str() , NULL, NULL)},
   _readtopic{dds_create_topic(_dds_participant, &MSGData_Msg_desc, ("pingpong"+std::to_string(id)).c_str(), NULL, NULL)},
   _writer{dds_create_writer(_dds_participant, _writetopic, NULL, NULL)},
   _waitSet{dds_create_waitset(_dds_participant)},
   _id{id}
  {   
   if(pingpongmodifier > 0){
      //ping only
      //create empty dds listener
      _listener = dds_create_listener(NULL);
      //set the data available listener to the data_available function of the class
      dds_lset_data_available(_listener, data_available); 
      //create a reader to read from te readtopic with the listener defined
      _reader = dds_create_reader(_dds_participant, _readtopic, NULL, _listener);
      std::cout << "listener set" << std::endl;
   }
   else{
      //pong only
      //set listener to null will make sure no listener is set when creating the reader
      _listener = NULL;
      _reader = dds_create_reader(_dds_participant, _readtopic, NULL, _listener);
      if (_reader < 0){
         DDS_FATAL("dds_create_reader: %s\n", dds_strretcode(-_reader));
      }
      //create a readcondition for only unread data to be read
      dds_entity_t rdcond = dds_create_readcondition(_reader, DDS_SST_NOT_READ);
      if(rdcond < 0){
         DDS_FATAL("dds_create_readcondition: %s\n", dds_strretcode(-rdcond));
      }
      int status;
      //attach the readcondition to the reader and waitset
      status = dds_waitset_attach(_waitSet, rdcond, _reader);
      if (status < 0){
         DDS_FATAL("dds_waitset_attach: %s\n", dds_strretcode(-status));
      }  
   }
}
/**
 * @brief Function to read from the readtopic of the class, returns whether or not data was read.
 * 
 * The function allocates memory for a buffer in which possible read data can be stored.
 * It then tries to read data from the topic, if data was found it'll take the last message and write it to the buffer.
 * The dds_take function uses the class's reader to try and read data, the sample is the buffer in which the result will be written.
 * The info parameter contains the dds specified message information of the message(sample).
 * The last two parameters specify the buffer size and the maximum amount of samples to be taken.
 * 
 * If there was data read it'll cast the void pointer to a MSGData_Msg so you are able to acces the data as the message structure.
 * It'll then show the incoming message in the terminal, will free the memory of the read message and return true because there was data read.
 * 
 * If there wasn't any message to be read the allocated memory will be freed and false will be returnend because there was no data read.
 * 
 * @return true (there was data read)
 * @return false (thera was no data read)
 */
bool PingPong::read(){
   //declare dds return, buffer, info buffer, pointer for structure
   dds_return_t read_return_values;
   MSGData_Msg *msg;
   void *sample[1];
   dds_sample_info_t info[1];
   //allocate memory for the message
   sample[0] = MSGData_Msg__alloc();  
   //read from the topic
   read_return_values = dds_take(_reader, sample, info, 1, 1);
   //if there is data cast it to the stucture and print the message, afterwards free the allocated memory
   if(info[0].valid_data && (read_return_values > 0)){
      msg = (MSGData_Msg*) sample[0];
      std::cout << _id <<" incomingmessage: " << msg->message << std::endl;
      MSGData_Msg_free(sample[0], DDS_FREE_ALL);
      return true;
   }
   //no data to read free memory
   MSGData_Msg_free(sample[0], DDS_FREE_ALL);
   return false;
}
/**
 * @brief Function to write a string to the class's writetopic
 * 
 * this function creates a MSGData_Msg and sets its values 
 * to write it to the write topic of the class.
 * It uses the writer which was set in the class constructor
 * and the dds_write function from the dds API to do so.
 * 
 * @param message[IN] message string to be written to the topic
 */
void PingPong::write(std::string message){
   //define the message to send
   MSGData_Msg msg;
   msg.msgID = _id;
   msg.message = &message[0];
   std::cout << _id <<" sending: " << message << std::endl;
   //send the message
   dds_write(_writer, &msg);
}
/**
 * @brief listener function on data_available
 * 
 * This function notifies the user that the listener function has been called by
 * writing to the terminal.
 * 
 * @param reader[IN] the reader which called the listener function
 * @param arg[IN] args sent by the dds middleware ?
 */
void PingPong::data_available(dds_entity_t reader, void *arg){
   std::cout << "listened" << std::endl;
   std::fflush(stdout);
}

dds_entity_t PingPong::getWaitSet() const{
   return _waitSet;
}

PingPong::~PingPong() {
}



