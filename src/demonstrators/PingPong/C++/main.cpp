#include "dds/dds.h"
#include "MSGData.h"
#include "src/ping_pong.hpp"
#include <iostream>
#include <map>

int getMatrixboardIdFromUser();
int getPingOrPongFromUser();
std::string getMessageFromUser();

int main(int argc, char const *argv[]){          
    int id, pingorpong, modifier, status;
    std::string message;
    //get parameters from user to start
    std::cout << "Start " << std::endl;
    pingorpong = getPingOrPongFromUser();
    modifier = pingorpong == 1 ? 1 : -1;
    id = getMatrixboardIdFromUser();
    message =  getMessageFromUser();
    std::cout << "init" << std::endl;
    //initiate PingPong class
    PingPong ping_pong = PingPong(id, modifier);
    // wait for dds to settle before sending message
    dds_sleepfor(DDS_MSECS (200));
    if(pingorpong == 1){
        //send initial ping using the pingpong class write function
        ping_pong.write(message);
    }
    //size of the waitset results attach buffer
    dds_attach_t wsresults[1];
    //size of the waitset result
    size_t wsresultsize = 1U;
    //declare dds duration for wait set as infinite
    dds_duration_t waitTimeout = DDS_INFINITY;

    while(true){
        //flush to make sure it is printed
        std::fflush(stdout);
        //ping shows reading new data by polling
        if(pingorpong == 1){
            //pol for new data
            if(ping_pong.read()){
                //sleep 500 ms before sending reply
                dds_sleepfor(DDS_MSECS(500));
                ping_pong.write(message);
            } 
        }
        //pong shows reading new data by using a waitset
        else{
            std::cout << "pong waiting for ping" << std::endl;
            //waiting for new unread data
            status = dds_waitset_wait(ping_pong.getWaitSet(), wsresults, wsresultsize, waitTimeout);
            //error check
            if (status < 0){
                DDS_FATAL("dds_waitset_wait: %s\n", dds_strretcode(-status));
            }
            else{
                //read data
                ping_pong.read();
                //sleep 500 ms before sending reply
                dds_sleepfor(DDS_MSECS(500));
                ping_pong.write(message);
            }      
        }                  
    }
    
    std::cout << "Done" << std::endl;
    return 0;
}

int getMatrixboardIdFromUser(){
    int id = 0;
    std::cout << "Type an ID" << std::endl;
    std::cin >> id;
    return id;
}
int getPingOrPongFromUser(){
    int pingorpong = 0;
    std::cout << "1 = ping 2 = pong" << std::endl;
    std::cin >> pingorpong;
    if(pingorpong != 1 && pingorpong != 2){
        std::cout << "not 1 or 2 exiting..." << std::endl;
        return -1;
    }
    return pingorpong;
}
std::string getMessageFromUser(){
    std::string message;
    std::cout << "Type a message to send max 10 characters" << std::endl;
    std::cin >> message;
    return message;
}

