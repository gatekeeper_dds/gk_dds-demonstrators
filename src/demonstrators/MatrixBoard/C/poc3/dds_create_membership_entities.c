#include "MBCData.h"
#include "dds/dds.h"
#include "dds_create_membership_entities.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* This function creates a topic given the MBC's number and the participant and returns the topic's handle. */
dds_entity_t create_membership_topic(dds_entity_t participant)
{
  dds_entity_t topic;
  dds_qos_t* qos;
  qos = dds_create_qos();
  dds_qset_lifespan(qos, DDS_SECS(5));
  topic = dds_create_topic(participant, &MBCData_Membership_desc, "Membership", NULL, NULL);
  if (topic < 0)
  {
    DDS_FATAL("dds_create_topic: %s\n", dds_strretcode(-topic));
  }
  dds_delete_qos(qos);
  return topic;
}

/* This function creates a reader on a given topic and returns the reader's handle. */
dds_entity_t create_membership_reader(dds_entity_t participant, dds_entity_t topic, dds_listener_t* listener)
{
  dds_entity_t reader;
  reader = dds_create_reader(participant, topic, NULL, listener);
  if (reader < 0)
  {
    DDS_FATAL("dds_create_reader: %s\n", dds_strretcode(-reader));
  }
  return reader;
}

/* This function creates a writer on a given topic and returns the writer's handle. */
dds_entity_t create_membership_writer(dds_entity_t participant, dds_entity_t topic)
{
  dds_entity_t writer;
  writer = dds_create_writer(participant, topic, NULL, NULL);
  if (writer < 0)
  {
    DDS_FATAL("dds_create_write: %s\n", dds_strretcode(-writer));
  }
  return writer;
}