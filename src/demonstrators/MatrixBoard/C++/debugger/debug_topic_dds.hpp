#ifndef DEBUG_TOPIC_DDS_HPP
#define DEBUG_TOPIC_DDS_HPP

#include "MBCData.h"
#include "dds/dds_basics.hpp"
#include <dds/dds.h>

#include <string>

namespace Debug {

/**
 * @brief the communication with the debug topic using DDS
 *
 */
class DebugTopicDDS : public DDSBasics<DebugData_DebugInfo> {
  public:
    DebugTopicDDS(dds_entity_t *participant, const std::string &topicName);
    ~DebugTopicDDS() = default;

    void initialize(const bool isWriter, const bool isReader);

  private:
    const std::string _topicName;

    void configureQoS(dds_qos_t *qos);
};

} // namespace Debug

#endif