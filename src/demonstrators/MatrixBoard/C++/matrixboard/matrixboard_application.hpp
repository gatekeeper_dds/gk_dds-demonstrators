#ifndef MATRIXBOARD_APPLICATION_H
#define MATRIXBOARD_APPLICATION_H

#include <atomic> //atomic_bool
#include <memory> // unique_ptr
#include <string>
#include <unordered_map>

#include "dds/dds.h"

#include "MBCData.h"
#include "matrixboard/car_sensor.hpp"
#include "matrixboard/matrixboard_display.hpp"

#include "matrixboard/dds/active_matrixboard_manager.hpp"
#include "matrixboard/dds/specific_matrixboard.hpp"

/**
 * @brief contains the functions for the matrixboard application
 *
 */
class MatrixBoardApplication {
  public:
    // Constructor & Destructor
    explicit MatrixBoardApplication(const unsigned int id);
    ~MatrixBoardApplication();

    // Start and stop functions
    void run();
    static void stop() { _stop = true; }

    static void checkDDSerror(const int32_t value);

  private:
    static std::atomic_bool _stop;
    const unsigned int _id;
    unsigned int _oldTraffic;
    dds_entity_t _participant;

    using milliseconds = int;
    static constexpr milliseconds _trafficTime = 1000;

    void createDDSparticipant();
    void removeDDSparticipant();

    MBCData_Msg createMessage(const std::string &message) const;
    void updateTraffic(unsigned int *traffic1, unsigned int *traffic2);
    void updateSubsequentBoards();
    void removeOldBoards();

    matrixboard::ActiveMatrixBoardManager _activeMB;
    matrixboard::SpecificMatrixBoard _ownMatrixboard;

  protected:
    matrixboard::MatrixBoardDisplay display_;
    matrixboard::CarSensor sensor_;

    // std::unique_ptr<matrixboard::SpecificMatrixBoard> _pSubsequentBoard;
    // std::unique_ptr<matrixboard::SpecificMatrixBoard> _pAfterSubsequentBoard;
    matrixboard::SubsequentMatrixBoards _subsequentBoardIDs;
    std::unordered_map<unsigned int, std::unique_ptr<matrixboard::SpecificMatrixBoard>> _subsequentBoards;

    unsigned int calculateSpeed(const unsigned int traffic);
};

#endif // !MATRIXBOARD_H