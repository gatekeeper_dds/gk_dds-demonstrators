#ifndef FILEMANAGER_HPP
#define FILEMANAGER_HPP

#include <fstream>
#include <sstream>
#include <string>

/**
 * @brief Manages a file
 *
 */
class FileManager {
  public:
    explicit FileManager(const std::string &fileName);
    ~FileManager() = default;

    template <typename... Args>
    void storeCSV(const Args &... args) const;

    void storeLine(const std::string &line) const;

  private:
    std::string _fileName;

    template <typename... Args>
    static std::string argToCSV(const Args &... args);
};

/**
 * @brief stores arguments as comma separated values in the file
 *
 * @param args the arguments that need to be stored
 */
template <typename... Args>
void FileManager::storeCSV(const Args &... args) const {
    std::fstream file{_fileName, std::fstream::in | std::fstream::app};
    file << argToCSV(args...) << "\n";
}

/**
 * @brief converts arguments to a comma separated values string
 *
 * @param args the arguments that need to be converted to a CSV string
 * @return std::string the string containing the arguments with a comma in between
 */
template <typename... Args>
std::string FileManager::argToCSV(const Args &... args) {
    std::ostringstream ss;
    ((ss << args << ", "), ...);
    return ss.str();
}

#endif