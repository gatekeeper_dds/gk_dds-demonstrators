#include "file_manager.hpp"

/**
 * @brief Constructs a new FileManager object
 * 
 * @param fileName the name of the file
 */
FileManager::FileManager(const std::string &fileName) : _fileName{fileName} {}

/**
 * @brief stores a line in the file
 * 
 * @param line the stored line in the file
 */
void FileManager::storeLine(const std::string &line) const {
    std::fstream file{_fileName, std::fstream::in | std::fstream::app};
    file << line << "\n";
}