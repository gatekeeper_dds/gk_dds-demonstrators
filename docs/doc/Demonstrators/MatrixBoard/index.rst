.. _demonstrators_matrixboard:

##########################
Matrix Board Communication
##########################
The Matrix Board Communication DDS Demonstrator shows the application of the DDS
protocol in a dynamic environment. Each board should publish the traffic it senses.
Each matrix board should dynamically find the two closest upstream matrix boards
and subscribe to them so it receives the traffic values they sense. According,
to these two numbers the board should calculate the speed limit it will display.
The traffic sensors are just random number generators that have some lower and
upper bound. Similarly, the displayed speed limit starts from the maximum initial
limit and decrements or increments in discrete steps while never going above the
maximum.

.. uml::

   @startditaa

   /---------\   /---------\   /=--------\   /---------\   /---------\   /---------\   /---------\   /---------\
   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |
   |         |   |         |   |         |   |         |   |         |   |         |   |         |   |         |
   |  loc=1  |   |  loc=2  |   |  loc=3  |   |  loc=5  |   |  loc=8  |   | loc=13  |   | loc=21  |   | loc=34  |
   |    c1F7 |   |    c1FF |   |         |   |    c1FF |   |    c1F7 |   |    c1FF |   |    cFDD |   |    c1FF |
   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/
        |             |             |             |             |             |             |             |
      --*-------------*-------------*-------------*-------------*-------------*-------------*-------------*-----> drive
   @endditaa

Implementation
""""""""""""""
The Matrix Board Communication currently has the following implementations. Note
that in some languages there are multiple proof of concepts in order to show that
the dynamic aspect can be implemented in various ways.

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    MatrixBoardC
    MatrixBoardCpp
    MatrixBoardPython