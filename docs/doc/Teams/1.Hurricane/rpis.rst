.. _rpis:

Setting up Raspberry Pi network
-------------------------------

In order to create a Raspberry Pi network, one of the Raspberry Pis will be used as a wifi router where all the other devices will connect to.

The first step is to install DNSMasq and HostAPD using::

   $ sudo apt install dnsmasq hostapd

Turn these two processes off::

   $ sudo systemctl stop dnsmasq
   $ sudo systemctl stop hostapd

Configuring a static IP
"""""""""""""""""""""""

To configure the static IP address, edit the dhcpcd configuration file with::

   $ sudo nano /etc/dhcpcd.conf

Go to the end of the file and edit it as follows:

.. code-block:: Console

  	interface wlan0
            static ip_address=192.168.4.1/24
            nohook wpa_supplicant

Restart the dhcpcd daemon using:: 

   $ sudo service dhcpcd restart

Configuring the DHCP server
"""""""""""""""""""""""""""

First move the old configuration file into the ``.orig`` file and start a new one::

   $ sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
   $ sudo nano /etc/dnsmasq.conf

Add the following two lines 

.. code-block:: Console

    interface=wlan0      # Use the require wireless interface - usually wlan0
    dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h

Interface ``wlan0`` will provide IP addresses between 192.168.4.2 and 192.168.4.20.

Start the ``dnsmasq``::

   $ sudo systemctl start dnsmasq



Configuring the access point
""""""""""""""""""""""""""""

Run the following command to edit the ``hostapd`` configuration file::

   $ sudo nano /etc/hostapd/hostapd.conf

In case the file does not exist simply create it at the above mentioned location and add the following lines:

.. code-block:: Console

    interface=wlan0
    driver=nl80211
    ssid=PID3
    hw_mode=g
    channel=7
    wmm_enabled=0
    macaddr_acl=0
    auth_algs=1
    ignore_broadcast_ssid=0
    wpa=2
    wpa_passphrase=Sogeti2020
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=TKIP
    rsn_pairwise=CCMP

Now we need to tell the system where to find the above mentioned file by::

    $ sudo nano /etc/default/hostapd

By changing the following line to:

.. code-block:: Console

    DAEMON_CONF="/etc/hostapd/hostapd.conf"

Start up
""""""""

Finally start all the required services by running::

    $ sudo systemctl unmask hostapd
    $ sudo systemctl enable hostapd
    $ sudo systemctl start hostapd

Routing and masquerade
"""""""""""""""""""""""

Edit ``/etc/sysctl.conf`` and uncomment this line:

.. code-block:: Console

    net.ipv4.ip_forward=1

Add a masquerade for outbound traffic on eth0 and save the iptables rule::

   $ sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
   $ sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

Edit ``/etc/rc.local`` and add this just above "exit 0" to install these rules on boot::

   $ iptables-restore < /etc/iptables.ipv4.nat

Reboot and check if it works by connecting any device with wifi to this new network with the ``SSID`` and ``password`` provided in the ``hostapd.conf`` file. 


Now it is possible to use ``ssh`` on that network to connect to any of the devices on the network including the router node. 

IP addresses
""""""""""""

The following IP addresses or hostnames with the corresponding passwords can be used to ``ssh`` into the Raspberry Pis that connect to the wifi network with ``SSID = PID3`` on ``raspberrypi-202``. 

.. list-table:: 
   :widths: 50 50 50
   :header-rows: 1

   * - Hostname
     - IP address
     - Password
   * - raspberrypi-200
     - 192.168.4.3
     - 200
   * - raspberrypi-201
     - 192.168.4.6
     - 201
   * - raspberrypi-202
     - 192.168.4.1
     - 202
   * - raspberrypi-203
     - 192.168.4.20
     - 203
   * - raspberrypi-204
     - 192.168.4.12
     - 204
   * - raspberrypi-205
     - 192.168.4.13
     - 205
   * - raspberrypi-206
     - 192.168.4.14
     - 206



Links
"""""
* Raspberry Pi as an access point (NAT): https://tinyurl.com/y3byvmjz


