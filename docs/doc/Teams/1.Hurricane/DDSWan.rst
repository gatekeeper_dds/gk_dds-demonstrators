.. _DDSWan:

Deploying DDS Over WAN
----------------------

In this section we are going to discuss how we can deploy Cyclone DDS over WAN.
We can think of many interesting use cases in which we would like our DDS applications
to be able to communicate with each other across the wider network. Although many
of the commercial DDS implementations include extensive documentation on how to
configure the DDS library through a ``.xml`` configuration file in order for applications
to be able to communicate over the wider network, the Cyclone DDS documentation is
still lacking the explanation required for us to implement such a scenario.
Furthermore, some features are still not implemented in Cyclone DDS since there
is no official release of this library. As a result, performing the across-network
discovery, which is the main issue in our case, can prove quite difficult without
utilizing some sort of common networking method.

In order to make sure that both remote nodes, which we wanted to connect, could
discover each other it was decided to create a VPN network using just a simple
online service. This VPN service focuses on IoT lightweight networking solutions.
By connecting both nodes (in this case 2 Raspberry Pis with different internet
connections) to this VPN network we could move to configuring our discovery protocol.
The discovery can be done in a similar manner to the way we choose our DDS network
interface, namely through the same ``cyclonedds.xml`` file. First, we find the
IPv6 addresses provided by our VPN network to each individual node and we add them
in our configuration by adding the following block.

.. code-block:: Console

        <Discovery>
            <Peers>
                <Peer address="[IPV6-address of local machine]"/>
                <Peer address="[IPV6-address of remote machine]"/>
            </Peers>
            <ParticipantIndex>auto</ParticipantIndex>
        </Discovery>

We add both IPv6 address to make sure that our configuration file can actually link
our local machine to the remote. We can also add the IPv4 addresses, but Cyclone
DDS does not allow IPv4 and IPv6 address mixing. Finally, we disable multicast since
it is not supported by the VPN service and add all these blocks to our final configuration
file.

.. code-block:: Console

    <?xml version="1.0" encoding="UTF-8" ?>
    <CycloneDDS xmlns="https://cdds.io/config" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://cdds.io/config https://raw.githubusercontent.com/eclipse-cyclonedds/cyclonedds/master/etc/cyclonedds.xsd">
        <Domain id="any">
            <General>
                <NetworkInterfaceAddress>auto</NetworkInterfaceAddress>
                <AllowMulticast>false</AllowMulticast>
                <MaxMessageSize>65500B</MaxMessageSize>
            <FragmentSize>4000B</FragmentSize>
            <Transport>udp6</Transport>
            </General>
        <Discovery>
            <Peers>
                <Peer address="[IPV6-address]"/>
                <Peer address="[IPV6-address]"/>
            </Peers>
            <ParticipantIndex>auto</ParticipantIndex>
        </Discovery>
            <Internal>
                <Watermarks>
                    <WhcHigh>500kB</WhcHigh>
                </Watermarks>
            </Internal>
            <Tracing>
                <Verbosity>severe</Verbosity>
                <OutputFile>stdout</OutputFile>
            </Tracing>
        </Domain>
    </CycloneDDS>

An important conclusion that was drawn from the WAN experiment was that we need
to add softer QoS constraints into our application in order for the communication to function
properly. For example, ``poc3`` of the Matrix Board Communication demonstrator
does not function consistently well, mainly due to the fact that the communication
is extremely fast and expects that the samples require deletion after a few milliseconds.
In the WAN case, those milliseconds are not enough for the receiving node to actually
read the message. In all other demonstrators where the communication is much slower
this issue was not noticed. In conclusion, adding such functionality requires the developer
to re-think his DDS timing constraints and add more relaxed QoS standards.

Links
"""""
* Using Cyclone DDS with Husarnet on ROS2: https://husarion.com/tutorials/other-tutorials/husarnet-cyclone-dds/