.. _DDSbaremetal_findings:

###########################################
DDS on bare-metal embedded hardware devices
###########################################

:authors: Furkan Ali Yurdakul
:date: Feb 2020

******************
Possible solutions 
******************

Research was done concerning different solutions for using the DDS communication 
protocol in an embedded environment. Each result will be compared to the 
requirements listed underneath, these were discussed with the project owner. 

Requirements: 

  * It must be easily accessible for an average user. For example: a known website, 
    in stock and doesn’t take more than a week to get it home. 
  * This solution will have to make use of the TCP/IP communication since this is 
    the base of the DDS communication protocol. 
  * The hardware must be relatively cheap (max €50). For example: affordable for 
    the average student.

To be able to do this, each solution will have a small summary to create an 
insight about it. Afterwards there will be a small hypothesis to see if the 
solution can be used to finish the project within a reasonable timeframe and 
fits the requirements. The estimated time to fully implement the solution will 
be given in days and is estimated by experience, knowledge and the amount of 
work it will need.  

SLIP/PPP
========
**Summary of the solution**

SLIP and PPP are known for implementing TCP/IP protocol in embedded hardware to 
communicate over serial ports. These protocols have been used a lot in the past 
and implementation enabled communication over TCIP/IP in most cases. Because this 
is just about the communication and not the implementation of the DDS communication 
protocol, this would mean that research has to be performed about the calls that 
are used to send or receive within the DDS communication protocol.

PPP is a newer version of SLIP but also more complex to use. The complexity will 
need a lot more storage on your embedded hardware compared to SLIP. PPP also has 
more configurable options like authentication, compression and encryption. However, 
in this project all that’s needed is a TCP/IP connection, so SLIP will be preferable 
in this case.

**Hypothesis**

.. sidebar:: Reference
  
  `SLIP <https://en.wikipedia.org/wiki/Serial_Line_Internet_Protocol>`_ |br|
  `SLIP Arduino implementation <https://playground.arduino.cc/Code/SerialIP/>`_ |br| 
  `PPP <https://en.wikipedia.org/wiki/Point-to-Point_Protocol>`_

This solution would be great to implement since it would make it possible to use 
DDS communication protocol in a wide variety of embedded hardware. These protocols 
are made to be used by everyone and the specifications are easy to find. Since it’s 
a protocol which exists over a longer period, people can find many implementations 
for known embedded hardware like Arduino. These protocols on their own don’t have 
any costs, only the hardware will be calculated in the cost. The estimated amount 
of days it will cost to finish this project using this solution is 60 days.

MicroPython/Pyboard
===================
**Summary of the solution**

.. image:: PyBoard.png
   :scale: 50 %
   :alt: Pyboard
   :align: right

MicroPython is an efficient implementation of the programming language Python 3 
with the standard libraries and is optimized to run on embedded hardware, 
specifically for the Pyboard. It also contains hardware-specific modules to make 
use of different low-level hardware, for example: I2C and GPIO. 

The Pyboard is a compact electronic circuit board that runs MicroPython on 
the bare metal, giving you a low-level Python operating system that can be 
used to control all kinds of electronic projects.

**Hypothesis**

.. sidebar:: Reference
  
  `Micropython/Pyboard <https://micropython.org/>`_ |br|
  `ESP8266 Micropython <https://www.hobbyelectronica.nl/product/esp8266-micropython/>`_

Its already known that DDS can be implemented with different kind of programming 
languages including Python. In this case the Pyboard only misses a connection made 
by the TCP/IP protocol. The Pyboard is available in online stores and has a lot of 
resources to fully understand how it works. The Pyboard itself costs €59 which makes 
it slightly more expensive than the cost requirements. The original Pyboard can be 
used with SLIP/PPP or the Pyboard "ESP8266 Micropython" can be used. The estimated 
amount of days it will cost to finish this project using this solution is 70 days.

Arduino + Ethernet
==================
**Summary of the solution**

.. image:: ArduinoUno.png
   :scale: 50 %
   :alt: Arduino Uno
   :align: right

.. image:: ArduinoEthernetShield.png
   :scale: 45 %
   :alt: Arduino Ethernet Shield
   :align: right

Arduino has many products in-house. For this project, only the most relevant 
will be considered. First, there is the standard Arduino UNO Rev3. It is very 
likely that a programmer has this kind of board and therefore familiar with the 
programming language C/C++. The disadvantage of this board is that there is no 
Ethernet port. It can use the SLIP protocol as mentioned earlier, or an Arduino 
Ethernet Shield 2 can be used. This is an add-on for the standard Arduino, with 
which you can use an Ethernet port for communication.

Honorable mentions; Arduino Ethernet w/o PoE and Arduino Yun Rev 2. 
Arduino Ethernet is a retired product and Arduino Yun is a Linux based product. 
Therefore, these are not suitable for this project. 

**Hypothesis** 

.. sidebar:: Reference
  
  `Arduino Uno Rev3 <https://store.arduino.cc/arduino-uno-rev3 |br|>`_ |br|
  `Arduino Ethernet Shield <https://store.arduino.cc/arduino-ethernet-shield-2>`_ |br|
  `Arduino Ethernet w/o PoE <https://store.arduino.cc/arduino-ethernet-rev3-without-poe>`_ |br|
  `Arduino Yun Rev 2 <https://store.arduino.cc/arduino-yun-rev-2>`_

On the forums of Arduino, it shows that SLIP has been implemented on Arduino. 
This makes it possible to use the standard Arduino without the ethernet shield. 
However, when using the ethernet shield time can be saved to investigate SLIP 
and its limitations. The Arduino Uno Rev3 costs 20 euro and the Ethernet Shield 
costs 25 euro, so it will cost 45 euros in total. The expected time for achieving 
the project with this solution is 60 days. 

Embedded RTPS - STM32
=====================

.. image:: Nucleo.png
   :scale: 70 %
   :alt: Nucleo
   :align: right

**Summary of the solution**

This is the only solution for which there is evidence that the DDS communication 
protocol has worked. This solution uses the microcontroller: STM32Nucleo-F767ZI. 
This Microcontroller has an Ethernet port at its disposal so there is no need 
for SLIP/PPP. According to the GitHub page, this microcontroller has the minimum 
to create a communication with a Linux device. According to this page, the 2 
devices can send at least 10 messages back and forth. 

**Hypothesis**

.. sidebar:: Reference
  
  `GitHub <https://github.com/embedded-software-laboratory/embeddedRTPS-STM32>`_ |br|
  `STM32Nucleo-F767ZI <https://nl.farnell.com/stmicroelectronics/nucleo-f767zi/dev-board-nucleo-32-mcu/dp/2546569>`_ 

Because this is a ready-made software package, it must first be investigated 
whether licenses are required for its use within this project. This concept 
solution will speed up the time to a result and more time can be spent on a 
well-made guide. The cost of this controller is 25 euros. It is expected that 
this can be achieved in 45 days.

Conclusion
----------

When looking at the requirements, one is less noticeable 
but affects a project that is time-limited.

For hardware solutions that can’t directly use TCIP/IP communication, 
SLIP or PPP can be used. Because implementing SLIP or PPP will take too much time, 
according to the hypothesis, a separate Arduino or standard PyBoard can’t be used. 
The Arduino + the Ethernet add on would we applicable, but is too expensive.

We also need to look at how realistic these solutions are. 
The sources indicate that the "Embedded RTPS - STM32" solution should be 
able to use DDS, which can be seen from the sources. After a short discussion, 
it was decided to use the Nucleo for the final result.

Check ":ref:`NucleoSetupExplanation`" to learn how to set up the environment for this device.