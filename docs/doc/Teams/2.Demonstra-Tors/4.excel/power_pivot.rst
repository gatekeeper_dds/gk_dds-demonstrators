.. _power_pivot:

Power Pivot
----------------

:authors: Sam Laan
:date: April 2020

Description
"""""""""""
This page shows information about using a CSV file with Power Pivot.

Enabling Power Pivot
""""""""""""""""""""
Power Pivot is not enabled by default in Excel and is also not available 
when using a CSV file.
To enable Power Pivot, the following steps should be followed:

* Press file in the top bar.
* Select options.
* Select Add-ins.
* Select COM Add-ins in the bottom dropdown and click go.
* Check the box next to Power Pivot for Excel.

Congratulations! you have enabled Power Pivot.

Importing a CSV File
""""""""""""""""""""
Power Pivot is capable of importing data from various sources.
These are the steps to import a CSV file.

* Choose Power Pivot in the top bar.
* Press manage
* Select Get External Data
* Select from other sources
* Scroll down and select Text File
* Browse and select the desired CSV
* Choose the applicable column separator
* Use the first row as headers if applicable
* Press finish

The CSV file will now be imported.
The data from the file can be refreshed using the refresh button.
To get a pivot table of your data on your Excel sheet press PivotTable.
This pivot table can be used to filter data and as a source for graphs.
You can also add measures.

Measures
""""""""
Measures can be used to create calculated columns within the data.
DAX syntax is used to create measures click on your pivot table.
To find out more about DAX syntax please look at the syntax reference
(https://docs.microsoft.com/nl-nl/dax/dax-syntax-reference).