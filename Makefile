default: all
all doc: docs


.PHONY: docs doc
docs:
	make -C docs $@

clean:
	make -C docs $@

cleaner: clean

veryclean cleanest: cleaner
	-rm -rf `find . -type d -name __pycache__` /dev/null


